step-ca
=========

Install and configure Step CA for secure, automated X.509 and SSH certificate management.

This role can be used to deploy Step-CA either as a single node or in high availability mode.


Requirements
------------

**A few things to consider when setting up Step-CA in HA mode:**
- Use a external database by providing the variable `step_ca_external_database`. See the [database documentation](https://smallstep.com/docs/step-ca/configuration/#databases)
- Synchronize the main configuration file `ca.json` across all instances or setup a shared filesystem such as `GlusterFS` or `NFS`.
- If the `ca.json` is modified (manually or using step cli commands) the other instances will not pick up<br> on this change until each instance got the updated file and the `step-ca.service` is reloaded or restarted.

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
# Step CA and CLI download URLs
step_ca_download_base_url: "https://github.com/smallstep/certificates/releases/download"
step_ca_download_local_url: "https://files.repository.mueller.de/step-ca"

step_cli_download_base_url: "https://github.com/smallstep/cli/releases/download"
step_cli_download_local_url: "https://files.repository.mueller.de/step-ca"

step_ca_version: "0.23.2" # Use latest as value to fetch the latest release from github (firewall access required) or provide specific version
step_cli_version: "0.23.2"

# Step CA preparation and installation config
step_ca_config_dir: /etc/step-ca
step_ca_bin: /usr/bin/step-ca
step_cli_bin: /usr/bin/step
step_ca_user: step-ca

# ----------------------------------------
# Basic Step CA intialization variables
# ----------------------------------------

# Step CA initialization config
step_ca_name: "Step"
step_ca_address: ":443" # Default bind address and port
step_ca_url: "https://{{ ansible_fqdn }}{{ step_ca_address }}" # Optional: Default address added to step-cli client defaults.json on the step-ca server
step_ca_acme: false # Create a default ACME provisioner

step_ca_dns: [] # Required list of all dns names for the ca certificate
#step_ca_dns:
#  - 10.199.34.85
#  - 10.199.34.86
#  - stepca.example.com
#  - stepca-01.example.com
#  - stepca-02.example.com

step_ca_intermediate_password: "" # Required encryption password for intermediate key (required for new or existing key) 

# Initial provisioner used when 'step_ca_remote_management' is set to false. Will be ignored when remote management is enabled
step_ca_provisioner: "step" # Initial JWK provisioner that will be created to request certificates

# Password used for initial JWK and JWK Admin user
step_ca_provisioner_password: "" 

# ----------------------------------------
# Remote Management variables
# ----------------------------------------

## When remote provisioner management is enabled, your provisioner configuration is stored in the database, rather than in ca.json config. 
## Run 'step ca provisioner' to manage provisioner configuration. These commands require you to sign in as an Admin user.
step_ca_remote_management: no

# Default claims on authority level in json format without brackets https://smallstep.com/docs/step-ca/configuration/#configuration-options
step_ca_authority_claims: |-
  "minTLSCertDuration": "5m",
  "maxTLSCertDuration": "24h",
  "defaultTLSCertDuration": "24h",
  "disableRenewal": false,
  "allowRenewalAfterExpiry": false

# ----------------------------------------
# Optional Step CA intialization variables
# ----------------------------------------

## Step-CA generates root and intermediate certificates automatically at initialization.
## However you can provide your own existing root and intermediate certs, keys and their encryption passwords.

#step_ca_existing_root_crt: "/tmp/root_ca.crt" # Optional: Copy existing Root CA cert to step-ca instead of generating new cert at step init.
#step_ca_existing_root_key: "/tmp/root_ca.key" # Optional: Copy existing Root CA key - Not required when Step CA should be used as intermediate CA only.
#step_ca_existing_root_key_password: "test" # Optional: Encryption password for Root CA key - Not required when Step CA should be used as intermediate CA.

#step_ca_existing_intermediate_crt: "/tmp/intermediate_ca.crt" # Optional: Copy existing intermediate cert instead of generating new cert at step init.
#step_ca_existing_intermediate_key: "/tmp/intermediate_ca.key" # Optional: Copy existing intermediate key instead of generating new cert at step init.

#step_ca_external_database: |- # Optional: Provide external Database connection params without brackets in json. https://smallstep.com/docs/step-ca/configuration/#databases
#  "type": "postgresql",
#  "dataSource": "postgresql://user:password@127.0.0.1:5432/",
#  "database": "myDBName"
```

**Remote Management:**

When `step_ca_remote_management` is enabled, parts of the configuration will be stored in the database, rather than in ca.json config.

Enable remote management if Ansible should manage the main `ca.json` via Jinja2 template.

Disable remote management if you wish to customize the `ca.json` yourself and Ansible should never touch this file in future executions.

The default `step_ca_provisioner` will be converted to a admin user `step`.<br>

<br>

**Root and Intermediate Certificates:**

Step-CA generates root and intermediate certificates automatically at initialization.

However you can provide your own existing root and intermediate certs, keys and their encryption passwords.<br> Private keys needs to be PEM encoded and encrypted with a passphrase. (See examples in `defaults/main.yml`)


Dependencies
------------
- No Dependencies

Example Inventory
------------

```yml
all:
  children:
    step-ca:
      hosts:
        cfg-stepca-01.test.2ln.mueller.de:
          ansible_host: 10.199.34.86
        cfg-stepca-02.test.2ln.mueller.de:
          ansible_host: 10.199.34.87
        cfg-stepca-03.test.2ln.mueller.de:
          ansible_host: 10.199.34.88

```

Example Playbook
----------------

```yml
# Install and configure glusterfs on 3 nodes, 1 brick each and 3 replicas
# Install Step-CA in HA mode with shared GlusterFS filesystem and PostgreSQL database
- hosts: step-ca
  become: yes
  gather_facts: yes
  roles:
    - role: glusterfs
      vars:
        glusterfs_bricks:
          - name: brick1
            device: /dev/vdb
            mountpoint: /data/brick1
        glusterfs_volumes:
          - name: gv0
            bricks: /data/brick1/gv0
            replicas: 3
            mountpoint: /mnt/step-ca
            rebalance: no
        glusterfs_peer_nodes:
          - fqdn: stepca-01.example.com
            address: 10.199.34.86
          - fqdn: stepca-02.example.com
            address: 10.199.34.87
          - fqdn: stepca-03.example.com
            address: 10.199.34.88
    - role: step-ca
      vars:
        step_ca_config_dir: /mnt/step-ca
        step_ca_name: "Mueller Step"
        step_ca_url: "https://{{ ansible_fqdn }}{{ step_ca_address }}" 
        step_ca_acme: true 
        step_ca_dns: 
          - 10.199.34.85
          - 10.199.34.86
          - 10.199.34.87
          - 10.199.34.88
          - stepca.test.2ln.mueller.de
          - cfg-stepca-01.test.2ln.mueller.de
          - cfg-stepca-02.test.2ln.mueller.de
          - cfg-stepca-03.test.2ln.mueller.de
        step_ca_intermediate_password: "{{ vault_step_ca_intermediate_password }}"
        step_ca_provisioner: "step" 
        step_ca_provisioner_password: "{{ vault_step_ca_provisioner_password }}" 
        step_ca_remote_management: yes
        step_ca_authority_claims: |-
          "minTLSCertDuration": "5m",
          "maxTLSCertDuration": "2160h",
          "defaultTLSCertDuration": "2160h",
          "disableRenewal": false,
          "allowRenewalAfterExpiry": false
        step_ca_external_database: |- 
          "type": "postgresql",
          "dataSource": "postgresql://{{ vault_step_ca_postgresql_db_user }}:{{ vault_step_ca_postgresql_db_password }}@10.199.34.84:5432/",
          "database": "stepca"
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko